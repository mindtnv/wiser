using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Wiser.Models
{
    [Table("Books")]
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }
        public int Order { get; set; }
        public bool IsCompleted { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime? CompletedDate { get; set; }
        public string Author { get; set; }
        [JsonIgnore]
        public User User { get; set; }
        [JsonIgnore]
        public int UserId { get; set; }
    }
}