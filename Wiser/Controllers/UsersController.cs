using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wiser.Auth;
using Wiser.Auth.Registration;
using Wiser.Controllers.RequestModels;

namespace Wiser.Controllers
{
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly DbContext _context;
        private readonly IRegistrationService _registrationService;

        public UsersController(DbContext context, IAuthService authService, IRegistrationService registrationService)
        {
            _context = context;
            _authService = authService;
            _registrationService = registrationService;
        }

        [HttpGet]
        [Route("/user")]
        [Authorize]
        public async Task<ActionResult<object>> Get()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.Name)?.Value ?? "-1");
            if (id == -1)
                return Unauthorized();

            var user = await _context.Users.SingleOrDefaultAsync(x => x.Id == id);
            if (user == null)
                return Unauthorized();

            return new
            {
                user.Id,
                user.Email,
                user.Name,
            };
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/user/login")]
        public async Task<ActionResult<object>> Login([FromBody] LoginRequestModel model)
        {
            var authenticationResponse = await _authService.AuthenticateAsync(new AuthenticationRequest
            {
                Email = model.Email,
                Password = model.Password,
            });
            if (authenticationResponse == null)
                return new UnauthorizedResult();

            Response.Cookies.Append("RefreshToken", authenticationResponse.RefreshToken,
                CreateRefreshTokenCookieOptions());

            return new
            {
                authenticationResponse.Token,
            };
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("/user/register")]
        public async Task<ActionResult<object>> Register([FromBody] RegisterRequestModel model)
        {
            var registrationResponse = await _registrationService.RegisterAsync(new RegistrationRequest
            {
                Email = model.Email,
                Name = model.Name,
                Password = model.Password,
            });
            if (registrationResponse.Status is RegistrationResponse.RegistrationStatus.UserAlreadyExist)
                return Conflict();

            if (registrationResponse.AuthenticationResponse == null)
                throw new NullReferenceException(nameof(registrationResponse.AuthenticationResponse));

            Response.Cookies.Append("RefreshToken", registrationResponse.AuthenticationResponse.RefreshToken,
                CreateRefreshTokenCookieOptions());

            return new
            {
                registrationResponse.AuthenticationResponse.Token,
            };
        }

        [AllowAnonymous]
        [Route("/user/token")]
        public async Task<ActionResult<object>> Token()
        {
            var receivedToken = Request.Cookies["RefreshToken"];
            if (receivedToken == null)
                return Unauthorized();

            var authenticationResponse = await _authService.AuthenticateAsync(receivedToken);
            if (authenticationResponse == null)
                return Unauthorized();

            Response.Cookies.Append("RefreshToken", authenticationResponse.RefreshToken,
                CreateRefreshTokenCookieOptions());
            return new
            {
                authenticationResponse.Token,
            };
        }

        [AllowAnonymous]
        [HttpDelete]
        [Route("/user/token")]
        public async Task<ActionResult> RevokeToken()
        {
            var receivedToken = Request.Cookies["RefreshToken"];
            if (receivedToken == null)
                return Unauthorized();

            var token = await _context.RefreshTokens.FirstOrDefaultAsync(x => x.Token == receivedToken);
            if (token == null)
                return Unauthorized();

            token.Revoked = DateTime.Now;
            Response.Cookies.Delete("RefreshToken");
            await _context.SaveChangesAsync();
            return Ok();
        }

        private CookieOptions CreateRefreshTokenCookieOptions()
        {
            var builder = new CookieBuilder
            {
                Name = "RefreshToken",
                HttpOnly = true,
                SameSite = SameSiteMode.Strict,
                SecurePolicy = CookieSecurePolicy.SameAsRequest,
            };
            return builder.Build(HttpContext);
        }
    }
}