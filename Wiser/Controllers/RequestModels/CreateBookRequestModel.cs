namespace Wiser.Controllers.RequestModels
{
    public class CreateBookRequest
    {
        public string Title { get; set; }
        public int PagesCount { get; set; }
        public string Author { get; set; }
    }
}