namespace Wiser.Controllers.RequestModels
{
    public class ReadRequest
    {
        public int PagesCount { get; set; }
    }
}