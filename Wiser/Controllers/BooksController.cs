using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wiser.Models;

namespace Wiser.Controllers
{
    [ApiController]
    [Authorize]
    [Route("/books")]
    public class BooksController : ControllerBase
    {
        private readonly DbContext _context;

        public BooksController(DbContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<object>> Get()
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            return new
            {
                Data = await _context.Books.Where(x => x.UserId == user.Id)
                                     .OrderBy(x => !x.IsCompleted)
                                     .ThenBy(x => x.Order)
                                     .ToArrayAsync(),
            };
        }

        private async Task<User?> GetUserAsync()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.Name)?.Value ?? "-1");
            if (id == -1)
                return null;

            var user = await _context.Users
                                     .Include(x => x.Books)
                                     .SingleOrDefaultAsync(x => x.Id == id);
            return user;
        }
    }
}