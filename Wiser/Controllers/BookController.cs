using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Wiser.Controllers.RequestModels;
using Wiser.Models;

namespace Wiser.Controllers
{
    [ApiController]
    [Authorize]
    [Route("/book")]
    public class BookController : ControllerBase
    {
        private readonly DbContext _context;

        public BookController(DbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<object>> Get([FromQuery] int offset = 0)
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var currentBook = await _context.Books.FirstOrDefaultAsync(x => x.Id == user.CurrentBookId);
            if (currentBook == null)
                return NotFound();

            var result = currentBook;
            if (offset < 0)
            {
                var before = await _context.Books
                                           .CountAsync(x => x.UserId == user.Id && x.Order < currentBook.Order);

                if (before < -offset)
                    return NotFound();

                result = await _context.Books
                                       .OrderByDescending(x => x.Order)
                                       .Where(x => x.UserId == user.Id && x.Order < currentBook.Order)
                                       .Skip(-offset - 1)
                                       .FirstAsync();
            }
            else if (offset > 0)
            {
                var after = await _context.Books
                                          .CountAsync(x => x.UserId == user.Id && x.Order > currentBook.Order);
                if (after < offset)
                    return NotFound();

                result = await _context.Books
                                       .OrderBy(x => x.Order)
                                       .Where(x => x.UserId == user.Id && x.Order > currentBook.Order)
                                       .Skip(offset - 1)
                                       .FirstAsync();
            }

            return new
            {
                Data = result,
            };
        }

        [HttpPost]
        public async Task<ActionResult<object>> Post([FromBody] CreateBookRequest request)
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            if (string.IsNullOrWhiteSpace(request.Title) | (request.PagesCount <= 0))
                return ValidationProblem();

            var lastBook = await _context.Books
                                         .OrderByDescending(x => x.Order)
                                         .FirstOrDefaultAsync(x => x.UserId == user.Id);
            var book = new Book
            {
                Order = (lastBook?.Order ?? 0) + 1,
                Title = request.Title,
                PagesCount = request.PagesCount,
                Author = request.Author,
                AddedDate = DateTime.Now,
            };
            user.Books.Add(book);
            await _context.SaveChangesAsync();
            if (lastBook == null || lastBook.IsCompleted)
            {
                user.CurrentBookId = book.Id;
                await _context.SaveChangesAsync();
            }

            return new
            {
                Data = book,
            };
        }

        [HttpPut]
        public async Task<ActionResult<object>> Put([FromBody] ReadRequest request)
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var book = await _context.Books.FirstOrDefaultAsync(x => x.Id == user.CurrentBookId);
            if (book == null)
                return Unauthorized();

            book.CurrentPage = Math.Clamp(book.CurrentPage + request.PagesCount, book.CurrentPage, book.PagesCount);
            if (book.CurrentPage == book.PagesCount)
            {
                book.IsCompleted = true;
                book.CompletedDate = DateTime.Now;
                await _context.SaveChangesAsync();
                await SetNextBook(user);
            }

            await _context.SaveChangesAsync();
            return new
            {
                Data = book,
            };
        }

        [HttpDelete]
        public async Task<ActionResult<object>> Delete([FromQuery] int id)
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var book = await _context.Books.FirstOrDefaultAsync(x => x.UserId == user.Id && x.Id == id);
            if (book == null)
                return NotFound();

            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            await SetNextBook(user);
            await _context.SaveChangesAsync();
            return new
            {
                Data = book,
            };
        }

        [HttpPatch]
        public async Task<ActionResult<object>> Patch([FromBody] UpdateBookRequestModel request)
        {
            var user = await GetUserAsync();
            if (user == null)
                return Unauthorized();

            var book = await _context.Books.FirstOrDefaultAsync(x => x.Id == request.Id);
            if (book == null)
                return NotFound();

            if ((request.CurrentPage > request.PagesCount) | (request.CurrentPage < 0) | (request.PagesCount < 0))
                return ValidationProblem();

            book.Title = request.Title ?? book.Title;
            book.CurrentPage = request.CurrentPage ?? book.CurrentPage;
            book.PagesCount = request.PagesCount ?? book.PagesCount;
            book.IsCompleted = request.IsCompleted ?? book.IsCompleted;
            if (book.Id == user.CurrentBookId && book.IsCompleted)
                await SetNextBook(user);

            await _context.SaveChangesAsync();
            return new
            {
                Data = book,
            };
        }

        private async Task SetNextBook(User user)
        {
            var nextBook = await _context.Books.OrderBy(x => x.Order)
                                         .FirstOrDefaultAsync(x => x.UserId == user.Id && !x.IsCompleted);
            user.CurrentBookId = nextBook?.Id ?? (await _context.Books.OrderByDescending(x => x.Order)
                                                                .FirstOrDefaultAsync(x => x.UserId == user.Id))
                ?.Id ?? -1;
        }

        private async Task<User?> GetUserAsync()
        {
            var id = int.Parse(User.FindFirst(ClaimTypes.Name)?.Value ?? "-1");
            if (id == -1)
                return null;

            var user = await _context.Users
                                     .Include(x => x.Books)
                                     .SingleOrDefaultAsync(x => x.Id == id);
            return user;
        }
    }
}