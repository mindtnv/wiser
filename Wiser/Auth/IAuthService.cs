using System.Threading.Tasks;

namespace Wiser.Auth
{
    public interface IAuthService
    {
        Task<AuthenticationResponse?> AuthenticateAsync(AuthenticationRequest request);
        Task<AuthenticationResponse?> AuthenticateAsync(string refreshToken);
    }
}