using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Wiser.Auth.JwtTokens;
using Wiser.Auth.RefreshTokens;

namespace Wiser.Auth
{
    public class AuthService : IAuthService
    {
        private readonly DbContext _context;
        private readonly IJwtTokenFactory _jwtTokenFactory;
        private readonly IRefreshTokenFactory _refreshTokenFactory;

        public AuthService(DbContext context, IJwtTokenFactory jwtTokenFactory,
            IRefreshTokenFactory refreshTokenFactory)
        {
            _context = context;
            _jwtTokenFactory = jwtTokenFactory;
            _refreshTokenFactory = refreshTokenFactory;
        }

        public async Task<AuthenticationResponse?> AuthenticateAsync(AuthenticationRequest request)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x =>
                x.Email == request.Email && x.Password == request.Password);
            if (user == null)
                return null;

            var refreshToken = _refreshTokenFactory.Create(TimeSpan.FromDays(1));
            user.RefreshTokens.Add(refreshToken);
            await _context.SaveChangesAsync();
            return new AuthenticationResponse
            {
                Id = user.Id,
                Token = _jwtTokenFactory.Create(user),
                RefreshToken = refreshToken.Token,
            };
        }

        public async Task<AuthenticationResponse?> AuthenticateAsync(string refreshToken)
        {
            var user = await _context.Users
                                     .Include(x => x.RefreshTokens)
                                     .SingleOrDefaultAsync(x => x.RefreshTokens.Any(t => t.Token == refreshToken));
            if (user == null)
                return null;

            var userRefreshToken = user.RefreshTokens.SingleOrDefault(x => x.Token == refreshToken && x.IsActive);
            if (userRefreshToken == null)
                return null;

            var newRefreshToken = _refreshTokenFactory.Create(TimeSpan.FromDays(1));
            user.RefreshTokens.Add(newRefreshToken);
            await _context.SaveChangesAsync();
            return new AuthenticationResponse
            {
                Id = user.Id,
                Token = _jwtTokenFactory.Create(user),
                RefreshToken = newRefreshToken.Token,
            };
        }
    }
}