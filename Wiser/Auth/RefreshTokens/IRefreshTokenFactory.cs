using System;

namespace Wiser.Auth.RefreshTokens
{
    public interface IRefreshTokenFactory
    {
        RefreshToken Create(TimeSpan expireAfter);
    }
}