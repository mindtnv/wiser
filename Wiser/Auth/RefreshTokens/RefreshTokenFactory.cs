using System;

namespace Wiser.Auth.RefreshTokens
{
    public class RefreshTokenFactory : IRefreshTokenFactory
    {
        public RefreshToken Create(TimeSpan expireAfter) =>
            new RefreshTokenBuilder().Expires(DateTime.UtcNow.Add(expireAfter)).Build();
    }
}