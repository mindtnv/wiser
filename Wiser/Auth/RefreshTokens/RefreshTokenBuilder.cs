using System;
using System.Security.Cryptography;

namespace Wiser.Auth.RefreshTokens
{
    public class RefreshTokenBuilder
    {
        private DateTime? _expires;

        public RefreshTokenBuilder Expires(DateTime dateTime)
        {
            if (dateTime <= DateTime.Now)
                throw new ArgumentOutOfRangeException(nameof(dateTime));

            _expires = dateTime;
            return this;
        }

        public RefreshToken Build()
        {
            if (_expires != null)
                return new RefreshToken
                {
                    Token = GenerateToken(),
                    Created = DateTime.UtcNow,
                    Expires = _expires.Value,
                };

            throw new NullReferenceException(nameof(_expires));
        }

        private static string GenerateToken()
        {
            var token = new byte[64];
            RandomNumberGenerator.Fill(token);
            return Convert.ToBase64String(token);
        }
    }
}