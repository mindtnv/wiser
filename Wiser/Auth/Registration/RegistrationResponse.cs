namespace Wiser.Auth.Registration
{
    public class RegistrationResponse
    {
        public enum RegistrationStatus
        {
            Ok,
            ValidationError,
            UserAlreadyExist,
        }

        public RegistrationStatus Status { get; set; }
        public AuthenticationResponse? AuthenticationResponse { get; set; }
    }
}