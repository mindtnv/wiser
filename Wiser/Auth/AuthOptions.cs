using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace Wiser.Auth
{
    public class AuthOptions
    {
        public string? Secret { get; set; }
        public string? Audience { get; set; }
        public string? Issuer { get; set; }
        public int? TokenLifetime { get; set; }
        public SymmetricSecurityKey? SecurityKey =>
            Secret != null ? new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret)) : null;
    }
}