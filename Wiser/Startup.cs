using System;
using System.Linq;
using System.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Wiser.Auth;
using Wiser.Auth.JwtTokens;
using Wiser.Auth.RefreshTokens;
using Wiser.Auth.Registration;

namespace Wiser
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DbContext>(builder =>
            {
                builder.UseNpgsql(_configuration.GetConnectionString("Default"));
            });
            services.AddControllers();
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder.AllowAnyHeader();
                    builder.AllowCredentials();
                    builder.AllowAnyMethod();
                    builder.WithOrigins(_configuration.GetSection("Cors").GetSection("Origins").Get<string[]>());
                });
            });
            AddJwtBearer(services);
            AddAuthServices(services);
            services.AddSwaggerGen(builder =>
            {
                builder.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.Http,
                    BearerFormat = "JWT",
                    Scheme = "Bearer",
                });
                builder.AddSecurityRequirement(new OpenApiSecurityRequirement());
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                ApplyMigrations(
                    app.ApplicationServices.GetService<DbContext>() ?? throw new InvalidOperationException());
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private void AddAuthServices(IServiceCollection services)
        {
            services.AddScoped<IRegistrationService, RegistrationService>();
            services.AddSingleton<IJwtTokenFactory, JwtTokenFactory>();
            services.AddSingleton<IRefreshTokenFactory, RefreshTokenFactory>();
            services.Configure<AuthOptions>(_configuration.GetSection(nameof(AuthOptions)));
            services.AddScoped<IAuthService, AuthService>();
        }

        public void ApplyMigrations(DbContext context)
        {
            if (context.Database.GetPendingMigrations().Any())
                context.Database.Migrate();
        }

        private void AddJwtBearer(IServiceCollection services)
        {
            var authOptions = _configuration.GetSection(nameof(AuthOptions)).Get<AuthOptions>() ??
                              throw new SecurityException($"{nameof(AuthOptions)} not configured");

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata = false;
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ClockSkew = TimeSpan.Zero,
                            ValidIssuer = authOptions.Issuer,
                            ValidAudience = authOptions.Audience,
                            ValidateAudience = true,
                            ValidateIssuer = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            RequireExpirationTime = true,
                            IssuerSigningKey = authOptions.SecurityKey,
                        };
                    });
        }
    }
}